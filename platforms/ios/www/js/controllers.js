angular.module('starter.controllers', [])

  .controller('languageSelectionCtrl', function ($scope) {
    console.log("languageSelectionCtrl")
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;

  })
  .controller('floorPlanCtrl', function ($scope, ) {
    console.log("floorPlanCtrl")
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;
  })
  // review controller --start---
  .controller('reviewCtrl', function ($scope, ) {
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;

    $scope.onClickHandler = function () {
      console.log("run")
      angular.element(document.getElementById("animate-spread-left")).addClass("slideOutLeft");
      angular.element(document.getElementById("animate-spread-right")).addClass("slideOutRight");
    }
  })
  .controller('reviewInformativeCtrl', function ($scope, $stateParams, getDataService) {
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;

    console.log($stateParams)

    $scope.building_type = $stateParams.building_type

    $scope.buldinInfo = getDataService.getReviewInfo($scope.building_type)

    console.log($scope.buldinInfo)
  })
  .controller('reviewMapCtrl', function ($scope, $stateParams, getDataService) {
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;

    console.log($stateParams)
    
    $scope.building = $stateParams.building
  })
  // review controller --end---
  .controller("MainController", function ($scope, $state, $rootScope, $ionicPlatform, $cordovaBeacon, ionicUI, $location) {

    $scope.routing = function (url) {

      alert(url)
      
      alert('Seed is ' + window.Invoke_params.seed)

      var routingUrl = url.split('//')[1]

      console.log(routingUrl) 

      var buildingParam = routingUrl.split('/')[1]
        
      console.log(buildingParamå)

      // if(routingUrl == "reviewInformative"){

      //   var buildingParam = routingUrl.split('/')[1]

      //   alert(buildingParam)
        
        $state.go(routingUrl, {building_type:0})

      // }else{

        // $state.go(routingUrl)   

      // }
    }
    $scope.beacons = "ProximityFar";

    $scope.popupAlready = false;
 
    $ionicPlatform.ready(function() {
 
        $cordovaBeacon.requestWhenInUseAuthorization();
 
        $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function(event, pluginResult) {
            // var uniqueBeaconKey;
            for(var i = 0; i < pluginResult.beacons.length; i++) {
              if(pluginResult.beacons[i].major == "2605"){
                // uniqueBeaconKey = pluginResult.beacons[i].uuid + ":" + pluginResult.beacons[i].major + ":" + pluginResult.beacons[i].minor;   
                $scope.beacons = "beacon 2605 is "+pluginResult.beacons[i].proximity     

                if( pluginResult.beacons[i].proximity == "ProximityImmediate"){
                   $scope.popupAlready = true;
                    var opt = {
                      cssClass: 'beaconDetect',                    
                      title: 'Consume Ice Cream',
                      template: "beacon 2605 is near to you",
                    }
                    ionicUI.popUpFun("confirm", opt).then(function(res) {
                      if(res) {
                        console.log('You are sure');
                        $state.go()
                      } else {
                        console.log('You are not sure');
                      }
                    });
                }   
              }
            }
            $scope.$apply();
        });
 
        $cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("estimote", "684E2388-303A-43AB-ADE8-3165B84B0ECA"));
 
    });

  })


  // revitalisationSlider  --start--
  .controller('revitalisationCtrl', function ($scope, $stateParams, $state, $timeout) {

    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;

    $scope.onClickHandler = function () {
      console.log("run")

      angular.element(document.getElementById("animate-spread-left")).addClass("slideOutLeft");
      angular.element(document.getElementById("animate-spread-right")).addClass("slideOutRight");
      $timeout(function () {
        $state.go("revitalisationSlider")
        angular.element(document.getElementById("animate-spread-left")).removeClass("slideOutLeft");
        angular.element(document.getElementById("animate-spread-right")).removeClass("slideOutRight");
      }, 800)
    }
  })
  .controller('revitalisationSliderCtrl', function ($scope, $stateParams, $state) {

    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;

    $scope.onClickHandler = function (opt) {
      switch (opt) {
        case 1:
          $state.go("revitalisationSlider")
          break;
        case 2:
          $state.go("revitalisationSlider2")
          break;
        case 3:
          $state.go("revitalisationSliderShow")
          break;
      }
    }
  })
  .controller('SliderShowCtrl', function ($scope, $ionicSlideBoxDelegate, getDataService) {
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;
    
    $scope.imgList = getDataService.getData("露台").slider

    $scope.slideChanged = function (index) {

      $ionicSlideBoxDelegate.enableSlide(false);

      $scope.slideIndex = index;

    };
  })
    .controller('VideoCtrl', function ($scope, $ionicSlideBoxDelegate, getDataService) {
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;
    
    $scope.imgList =getDataService.getData("露台").slider

    $scope.clipSrc ="movie/movie.mp4"


    $scope.slideChanged = function (index) {

      $ionicSlideBoxDelegate.enableSlide(false);

      $scope.slideIndex = index;

    };
  })
  .controller('revitalisationSliderBoxCtrl', function ($scope) {

    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;
    

    $scope.imgList = [
      { img: 'img/revitalisation/slider/露台.jpg', text: "露台" },
      { img: 'img/revitalisation/slider/外牆裝飾.jpg', text: "外牆裝飾" },
      { img: 'img/revitalisation/slider/樓梯頂通風.jpg', text: "樓梯頂通風" },
      { img: 'img/revitalisation/slider/露天後樓梯.jpg', text: "露天後樓梯" },
      { img: 'img/revitalisation/slider/PE_活化再用e.jpg', text: "活化再用" },
      
    ];
  })
  // revitalisationSlider  --end--
  //rehabilitation -- start 
  .controller("rehabilitationCtrl", function($scope){
     $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;
    $scope.imgList = [
      { img: 'img/revitalisation/slider/其他特色.jpg', text: "其他特色" },
      { img: 'img/revitalisation/slider/外牆裝飾.jpg', text: "外牆裝飾" },
      { img: 'img/revitalisation/slider/露天後樓梯.jpg', text: "露天後樓梯" },
    ];
    console.log("rehabilitationCtrl")
  })

  .controller("rehabilitationCMSCtrl", function($scope, getDataService){
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;

    $scope.data = []

    $scope.selectedIndex = 0 ;

    getDataService.getCMSData("js/rehabilation.json").then(function(data){
      console.log(data)
      $scope.data= data["cn"]
      $scope.selectedObj = data["cn"][$scope.selectedIndex]
      animation()
    })

    $scope.selectTopic = function(index){
      console.log(index)
      console.log($scope.data)
      setTimeout(function(){
          $scope.$apply( function() {
              $scope.selectedIndex = index;
              $scope.selectedObj = $scope.data[$scope.selectedIndex]  
              animation() 
          });
      }, 500);
    }
    var animation = function(){
      angular.element(document.getElementById("animatedHibilitation")).addClass("slideInDown");
      setTimeout(function(){
        angular.element(document.getElementById("animatedHibilitation")).removeClass("slideInDown");
      }, 1000);
    }
  })
 .controller("residentViewCtrl", function($scope, getDataService, $state){
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;
    getDataService.getCMSData("js/redevelopment.json").then(function(data){
      console.log(data)
      $scope.data= data["cn"]
      $scope.selectedObj = data["cn"][$scope.selectedIndex]
    })
    
    $scope.setResidentObj = function(obj){
      getDataService.setResidentObj(obj)
      $state.go("residentView2")
    }

  })
 .controller("residentView2Ctrl", function($scope, getDataService, $stateParams){
    $scope.iframeHeight = window.innerHeight;
    $scope.iframeWidtth = window.innerWeight;
    getDataService.getCMSData("js/redevelopment.json").then(function(data){
      console.log(data)
      $scope.data= data["cn"]
      console.log($scope.data[0])
      $scope.selectResident = $scope.data[0]
    })
    // $scope.selectResident = getDataService.getResidentObj()
  })

