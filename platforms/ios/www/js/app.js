// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'ngCordovaBeacon', 'ngAnimate', 'pascalprecht.translate'])

  .run(function ($ionicPlatform, $rootScope, $translate) {
    $ionicPlatform.ready(function () {
      
      $rootScope.language = false;
      
      $rootScope.setLang = function() {
        // You can change the language during runtime
        $rootScope.language = !$rootScope.language 

        var lang = ($rootScope.language)?"cn":"en";

        $translate.use(lang);
        
      };
    // handleOpenURL("urece://languageSelection")
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      ionic.Platform.fullScreen();
      if (window.StatusBar) {
        return StatusBar.hide();
      }
    });
  })
  .config(function ($ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(5);

    // // note that you can also chain configs
    $ionicConfigProvider.views.transition("none")

    // $ionicSideMenuDelegate.canDragContent(false)
  })
  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // first intro page and lange selection -- start 
    .state('languageSelection', {
        url: '/languageSelection',
        templateUrl: 'templates/languageSelection.html',
        controller: "languageSelectionCtrl"
      })
      .state('floorPlan', {
        url: '/floorPlan',
        templateUrl: 'templates/floorPlan.html',
        controller: "floorPlanCtrl"
      })
    // first intro page and lange selection -- end       

      // setup an abstract state for the tabs directive
      .state('revitalisation', {
        url: '/revitalisation',
        templateUrl: 'templates/revitalisation/revitalisation.html',
        controller: "revitalisationCtrl"
      })
      .state('revitalisationSlider', {
        url: '/revitalisationSlider',
        templateUrl: 'templates/revitalisation/slider.html',
        controller: "revitalisationSliderCtrl"
      })
      .state('revitalisationSlider2', {
        url: '/revitalisationSlider2',
        templateUrl: 'templates/revitalisation/slider2.html',
        controller: "revitalisationSliderCtrl"
      })
      .state('revitalisationSliderShow', {
        url: '/revitalisationSliderShow',
        templateUrl: 'templates/revitalisation/sliderShow.html',
        controller: "SliderShowCtrl"
      })
      .state('revitalisationVideo', {
        url: '/revitalisationVideo',
        templateUrl: 'templates/revitalisation/video.html',
        controller: "VideoCtrl"
      })
      .state('revitalisationSliderBox', {
        url: '/revitalisationSliderBox',
        templateUrl: 'templates/revitalisation/sliderBox.html',
        controller: "revitalisationSliderBoxCtrl"
      })


      // Each tab has its own nav history stack:

    // review --started--
      .state('review', {
        url: '/review',
        templateUrl: 'templates/review/review.html',
        controller: 'reviewCtrl'
      })

      .state('reviewInformative', {
        url: '/reviewInformative/:building_type',
        templateUrl: 'templates/review/reviewInformative.html',
        controller: 'reviewInformativeCtrl'
      })
      .state('reviewMap', {
        url: '/reviewMap/:building',
        templateUrl: 'templates/review/reviewMap.html',
        controller: 'reviewMapCtrl'
      })
      .state('residentView', {
        url: '/residentView',
        templateUrl: 'templates/review/residentView.html',
        controller: 'residentViewCtrl'
      })
      .state('residentView2', {
        url: '/residentView2',
        templateUrl: 'templates/review/residentView2.html',
        controller: 'residentView2Ctrl'
      })
    // review --end--
    //rehabilitation --start
    .state('rehabilitation', {
        url: '/rehabilitation',
        templateUrl: 'templates/revitalisation/revitalisation.html',
        controller: 'rehabilitationCtrl'
    })
    .state('rehabilitationCMS', {
        url: '/rehabilitationCMS',
        templateUrl: 'templates/rehabilitation/rehabilitation.html',
        controller: 'rehabilitationCMSCtrl'
    })
    //rehabilitation --end
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('languageSelection');

  })
  .config(['$translateProvider', function($translateProvider){
    // Adding a translation table for the English language
    $translateProvider.translations('cn', tranateFn().cn);
    // Adding a translation table for the Russian language
    $translateProvider.translations('en', tranateFn().en);
    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('cn');
  }])
  .filter('split', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(splitChar)[splitIndex];
        }
    });
  var handleOpenURL = function (url) {
      var body = document.getElementById("main");
      var scope = angular.element(body).scope();
      scope.routing(url)
  }
   var tranateFn= function(){
     
    return{
        cn:{
            "REVITALISATION_TITIE":"活化計劃",
            "REVITALISATION_BUILDING_WC":"灣仔茂蘿街／巴路士街",
            "REVITALISATION_BUILDING_PRINCE":"太子道西／園藝街",
            "REVITALISATION_BUILDING_PTL":"百子里活化計劃",
            "REVITALISATION_MORE_PLAN":"更多項目",
        },
        en:{
            "REVITALISATION_TITIE":"Revitalisation Project",
            "REVITALISATION_BUILDING_WC":"Mallory Street/Burrows Street Project",
            "REVITALISATION_BUILDING_PRINCE":"Prince Edward Road West/Yuen Ngai Street ",
            "REVITALISATION_BUILDING_PTL":"Pak Tsz Lane Revitalisation Project",
            "REVITALISATION_MORE_PLAN":"Other Revitalisation Project",
        }
        }   
  }