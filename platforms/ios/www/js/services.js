angular.module('starter.services', [])

  .factory('getDataService', function ($q, $http) {
    // Might use a resource here that returns a JSON array
    var selectResident={}
    // Some fake testing data
    var revitalidation = [
        {
          img: 'img/revitalisation/slider/露台.jpg',
          text: "露台",
          slider: [
            'img/revitalisation/slider/PE_活化再用.jpg',
            'img/revitalisation/slider/PE_活化再用a.jpg',
            'img/revitalisation/slider/PE_活化再用b.jpg',
            'img/revitalisation/slider/PE_活化再用c.jpg',
            'img/revitalisation/slider/PE_活化再用d.jpg',
          ]
        },
        {
          img: 'img/revitalisation/slider/其他特色.jpg',
          text: "其他特色",
          slider:[]
        },
        {
          img: 'img/revitalisation/slider/外牆裝飾.jpg',
          text: "外牆裝飾",
          slider:[]
        },
        {
          img: 'img/revitalisation/slider/露天後樓梯.jpg',
          text: "露天後樓梯",
          slider:[]
        },
    ];
    var reviewInfomation=[
      {
        buildingImg:"img/review/50_building.png",
        banner:"img/review/50_h.png",
        des:[
          '若缺乏妥善保養，結構及設施的失修情況嚴重',
          '舊樓宇設計不能符合現代標準',
          '衡量成本效益，以決定合適維修、改造重設或重建方案'
        ]
      },
       {
        buildingImg:"img/review/30-40_building.png",
        banner:"img/review/30-40_h.png",
        des:[
          '樓宇結構及設施老化',
          '全面勘察及適當維修可改善樓宇狀況',
          '改造重設」方案為樓宇增置符合現代標準的設施，提升居住質素'
        ]
      },
       {
        buildingImg:"img/review/30_building.png",
        banner:"img/review/30h.png",
        des:[
          '樓宇結構及設施狀況較佳',
          '大廈公契一般有界定樓宇管理和維修的責任，保養及維修較有系統',
          '進行定期檢查及預防性保養，可減慢樓宇老化速度'
        ]
      }
    ]
    return {
      getReviewInfo:function(index){
        return reviewInfomation[index]
      },
      getData:function(type){
        return _.find(revitalidation, {text:type})
      },
      setResidentObj:function(obj){
        console.log(obj)
        selectResident = obj
      },
      getResidentObj:function(obj){
       return selectResident
      },
      getCMSData:function(url){
         var deferred = $q.defer();
          $http.get(url).then(function(res){
            console.log(res)
            if(res.status == 200){
              deferred.resolve(res.data)
            }else{
              deferred.reject('Cant not get the data');
            }
          },
          function(reason) {
            deferred.reject('Failed: ' + reason);
          }
          )
        return deferred.promise;
      }
    };
  })
  .factory('URLService', function ($state) {
    function openUrltemplate(url) {
      $state.go(url);
    }
    return ({
      openUrltemplate: openUrltemplate
    });
  })
  .factory('ionicUI', function ($ionicPopup, ) {
    function popUpFun(type, opt) {
      console.log(type)
      console.log(opt)
      return $ionicPopup[type](opt)
    }
    return ({
      popUpFun: popUpFun
    });
  });

