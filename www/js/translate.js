var tranateFn= function(){
     
    return{
        cn:{
            "REVITALISATION_TITIE":"活化計劃",
            "REVITALISATION_BUILDING_WC":"灣仔茂蘿街／巴路士街",
            "REVITALISATION_BUILDING_PRINCE":"太子道西／園藝街",
            "REVITALISATION_BUILDING_PTL":"百子里活化計劃",
            "REVITALISATION_MORE_PLAN":"更多項目",
        },
        en:{
            "REVITALISATION_TITIE":"Revitalisation Project",
            "REVITALISATION_BUILDING_WC":"Mallory Street/Burrows Street Project",
            "REVITALISATION_BUILDING_PRINCE":"Prince Edward Road West/Yuen Ngai Street ",
            "REVITALISATION_BUILDING_PTL":"Pak Tsz Lane Revitalisation Project",
            "REVITALISATION_MORE_PLAN":"Other Revitalisation Project",
        }
      }   
  }